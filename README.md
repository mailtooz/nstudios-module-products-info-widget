### How to install:

1 Add the following into the "require" section of your composer.json file:

"nstudios/module-products-info-widget": "dev-master"

2 Run command 

bin/magento setup:upgrade
