define([
    'jquery',
    'NStudios_ProductsInfoWidget/js/aos.min',
], function ($, AOS) {
    $(document).ready(function () {
        AOS.init({
            offset: -50,
        });
        $("body").addClass('meet-the-squad');
    });
});