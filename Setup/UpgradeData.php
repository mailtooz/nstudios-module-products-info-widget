<?php

namespace NStudios\ProductsInfoWidget\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Api\AttributeManagementInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\TypeFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Catalog\Model\Product\Attribute\Frontend\Image as ImageFrontendModel;

/**
 * Class UpgradeData
 *
 * @package NStudios\ProductsInfoWidget\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $setup;

    /**
     * @var \Magento\Framework\Setup\ModuleContextInterface
     */
    private $context;

    /**
     * @var \Magento\Eav\Model\Entity\TypeFactory
     */
    private $typeFactory;

    /**
     * @var \Magento\Eav\Api\AttributeManagementInterface
     */
    private $attributeManagement;

    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param \Magento\Eav\Model\Entity\TypeFactory $typeFactory
     * @param \Magento\Eav\Api\AttributeManagementInterface $attributeManagement
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory
     */
    public function __construct(
        TypeFactory $typeFactory,
        AttributeManagementInterface $attributeManagement,
        EavSetupFactory $eavSetupFactory,
        CollectionFactory $collectionFactory
    ) {
        $this->typeFactory = $typeFactory;
        $this->attributeManagement = $attributeManagement;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;
        $this->context = $context;
        $this->setup->startSetup();
      
        if (version_compare($this->context->getVersion(), '1.0.4', '<')) {
            $this->addImageMediaAttribute();
            $this->addColouringImgAttribute();
            $this->addAdoptionCertImgAttribute();
        }

        if (version_compare($this->context->getVersion(), '1.0.5', '<')) {
            $this->addDownloadLinks();
        }

        $this->setup->endSetup();
    }

    private function addDownloadLinks()
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $eavSetup->removeAttribute(Product::ENTITY, 'ns_download_link');
        
        $eavSetup->addAttribute(
            Product::ENTITY,
            'ns_download_adoption_link',
            [
                'type' => 'varchar',
                'label' => 'Download Adoption Cert Link',
                'input' => 'text',
                'group' => 'General',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'ns_download_clr_sheet_link',
            [
                'type' => 'varchar',
                'label' => 'Download Colouring Sheet Link',
                'input' => 'text',
                'group' => 'General',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true
            ]
        );

        $this->assignAttributeToDefaultAttrSet('ns_download_adoption_link');
        $this->assignAttributeToDefaultAttrSet('ns_download_clr_sheet_link');
    }

    private function addImageMediaAttribute()
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'squishmallows_image',
            [
                'type' => 'varchar',
                'label' => 'SQM Image',
                'input' => 'media_image',
                'group' => 'General',
                'frontend' => ImageFrontendModel::class,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true
            ]
        );

        $this->assignAttributeToDefaultAttrSet('squishmallows_image');
    }

    private function addColouringImgAttribute()
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'sqm_colouring_image',
            [
                'type' => 'varchar',
                'label' => 'SQM Colouring Image',
                'input' => 'media_image',
                'group' => 'General',
                'frontend' => ImageFrontendModel::class,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true
            ]
        );

        $this->assignAttributeToDefaultAttrSet('sqm_colouring_image');
    }

    private function addAdoptionCertImgAttribute()
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'sqm_adoption_cert_image',
            [
                'type' => 'varchar',
                'label' => 'SQM Adoption Cert Image',
                'input' => 'media_image',
                'group' => 'General',
                'frontend' => ImageFrontendModel::class,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true
            ]
        );

        $this->assignAttributeToDefaultAttrSet('sqm_adoption_cert_image');
    }

    private function assignAttributeToDefaultAttrSet($attrId)
    {
        /** @var \Magento\Eav\Model\Entity\Type $entityType */
        $entityType = $this->typeFactory->create()->loadByCode('catalog_product');
        $defaultSetId = $entityType->getDefaultAttributeSetId();

        /** @var \Magento\Eav\Model\Entity\Attribute\Set $attributeSet */
        $attributeSet = $this->collectionFactory->create()
            ->addFieldToSelect('attribute_set_id')
            ->getFirstItem();

        try {
            $this->attributeManagement->assign(
                'catalog_product',
                $defaultSetId,
                $attributeSet->getDefaultGroupId($defaultSetId),
                $attrId,
                $attributeSet->getCollection()->getSize() * 10
            );
        } catch (\Exception $e) {
            return;
        }
    }
}
