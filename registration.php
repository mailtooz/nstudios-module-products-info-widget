<?php
/**
 * Copyright © 2015 NStudios. All rights reserved.
 * See COPYING.txt for license details.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'NStudios_ProductsInfoWidget',
    __DIR__
);
